<?php

namespace App\Http\Controllers;

use App\Voting_pannel;
use App\Party;
use App\Voter;
use App\Candidate;

use Illuminate\Http\Request;

class VotingPannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates=Candidate::all();
        return view('Voting_Pannel.index')->withCandidates($candidates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function voter_login()
    {
        return view('Voting_Pannel.voter_login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function voter_auth(Request $request)
    {
        $adhar=$request->adhar_no;
        $dob=$request->dob;
        $voters=Voter::all();
        foreach($voters as $voter)
        {
            if($voter->adhar_no==$adhar)
            {
                if($voter->dob==$dob)
                {
                    return redirect()->route('votings.index');
                }
                else{
                    return view('Voting_Pannel.voter_login')->with('danger','Invalid Voter');
                }
            }
            else{
                return view('Voting_Pannel.voter_login')->with('danger','Invalid Voter');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voting_pannel  $voting_pannel
     * @return \Illuminate\Http\Response
     */
    public function vote_save(Candidate $candidate)
    {
        return "you have Successfully voted";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voting_pannel  $voting_pannel
     * @return \Illuminate\Http\Response
     */
    public function edit(Voting_pannel $voting_pannel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voting_pannel  $voting_pannel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voting_pannel $voting_pannel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voting_pannel  $voting_pannel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voting_pannel $voting_pannel)
    {
        //
    }
}
