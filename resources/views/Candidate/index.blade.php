@extends('layouts.masterlayout')
@section('content')

   
    <a href="{{route('candidates.create')}}" class="btn btn-info" style="margin-left:700px">Add new Candidate</a>

    <table class="table table-dark" style="margin-top:50px">

    <tr>
      <th>Candidate ID</th>
      <th>Candidate Name</th>
      <th>State</th>
      <th>District</th>
      <th>Party</th>
      <th>Action</th>
    
    </tr>

@foreach($candidates as $candidate)
<tr>
    <td>{{$candidate->id}}</td>
    <td>
    {{$candidate->name}}
    </td>
    <td>
    {{$candidate->state}}
    </td>
    <td>
    {{$candidate->district}}
    </td>
    <td>
    {{$candidate->party->name}}
    </td>
    <td class="form-group row" >
    
    <a href="{{route('candidates.show',$candidate)}}" ><i class="fas fa-eye" style="margin-left:50px;color:#00BFFF"></i></button></a>
  
    <a href="{{route('candidates.edit',$candidate)}}" ><i class="fas fa-edit" style="margin-left:50px;color:#00BFFF"></i></button></a>
 
    
   
    <form action="{{route('candidates.destroy',$candidate)}}" method="POST">
    @csrf()
    @method('delete')
    <button style="margin-left:40px;color:#00BFFF; background-color:transparent; border:none" class="fas fa-trash"></button></td>
    </form>

    </tr>
    @endforeach

  </table>
@endsection