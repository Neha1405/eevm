<?php

namespace App\Http\Controllers;

use App\Voter;
use Illuminate\Http\Request;

class VoterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $voters=Voter::all();
        return view('Voter.index')->withVoters($voters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Voter.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $voter=new Voter;
        $voter->name=$request->name;
        $voter->adhar_no=$request->adhar_no;
        $voter->dob=$request->dob;
        $voter->state=$request->state;
        $voter->district=$request->district;
        $voter->save();
        return redirect()->route('voters.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Voter  $voter
     * @return \Illuminate\Http\Response
     */
    public function show(Voter $voter)
    {
        return view('Voter.show')->withVoter($voter);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Voter  $voter
     * @return \Illuminate\Http\Response
     */
    public function edit(Voter $voter)
    {
        return view('Voter.edit')->withVoter($voter);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Voter  $voter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voter $voter)
    {
        $voter->name=$request->name;
        $voter->state=$request->state;
        $voter->district=$request->district;
        $voter->adhar_no=$request->adhar_no;
        $voter->dob=$request->dob;

        $voter->save();
        return redirect()->route('voters.index');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Voter  $voter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voter $voter)
    {
        $voter->delete();
        return redirect()->route('voters.index');
    }
}
