<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voter extends Model
{
    public function candidates()
    {
        return $this->belongsTo('App\Candidate');
    }
}
