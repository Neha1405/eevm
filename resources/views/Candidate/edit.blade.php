@extends('layouts.masterlayout')
@section('content')


<form method="POST" action="{{route('candidates.update',$candidate)}}" > 
    @csrf()
    @method('PUT')
    <h1>Edit Candidate:</h1>
   
   
    <div class="form-group">
    <label >Candidate Name</label>
    <input type="text" class="form-control" name="name" value="{{$candidate->name}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>


<div class="form-group">
    <label >Candidate State:</label>
    <input type="text" class="form-control" name="state" value="{{$candidate->state}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Candidate District:</label>
    <input type="text" class="form-control" name="district" value="{{$candidate->district}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Candidate Party:</label>
    <input type="text" class="form-control" name="party" value="{{$candidate->party}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div>

  <button type="submit" class="btn btn-primary">Submit</button>
  </div>

</form>
@endsection