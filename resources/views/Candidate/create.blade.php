@extends('layouts.masterlayout')
@section('content')


<form method="POST" action="{{route('candidates.store')}}" > 
    @csrf()
    <h1>Create Candidate:</h1>
   
   
    <div class="form-group">
    <label >Candidate Name</label>
    <input type="text" class="form-control" name="name"  placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Candidate State:</label>
    <input type="text" class="form-control" name="state"  placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Candidate District:</label>
    <input type="text" class="form-control" name="district"  placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Candidate Party:</label>
    <select class="js-example-basic-multiple" name="party_id" style="width:100%">
  <option disabled>select Party</option>
  @foreach($parties as $party)
  <option value="{{ $party->id }}">{{ $party->name }}</option>
        @endforeach
</select>
</div><br>

<div>

  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
@endsection