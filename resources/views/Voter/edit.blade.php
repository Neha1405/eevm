@extends('layouts.masterlayout')
@section('content')


<form method="POST" action="{{route('voters.update',$voter)}}" > 
    @csrf()
    @method('PUT')
    <h1>Update Voter:</h1>
   
   
    <div class="form-group">
    <label >Voter Name</label>
    <input type="text" class="form-control" name="name" value="{{$voter->name}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Voter State:</label>
    <input type="text" class="form-control" name="state" value="{{$voter->state}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>

<div class="form-group">
    <label >Voter District:</label>
    <input type="text" class="form-control" name="district" value="{{$voter->district}}" placeholder="Enter name">
    <small  class="form-text text-danger"></small>
</div><br>


  <button type="submit" class="btn btn-primary">Submit</button>
  </div>

@endsection