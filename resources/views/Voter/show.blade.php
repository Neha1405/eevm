@extends('layouts.masterlayout')
@section('content')   
<h1>All Voters</h1>
   
    <a href="{{route('voters.create')}}" class="btn btn-info" style="margin-left:700px">Add new Voter</a>

    <table class="table table-dark" style="margin-top:50px">

    <tr>
      <th>Voter ID</th>
      <th>Voter Name</th>
      <th>State</th>
      <th>District</th>
    
    </tr>


<tr>
    <td>{{$voter->id}}</td>
    <td>
    {{$voter->name}}
    </td>
    <td>
    {{$voter->state}}
    </td>
    <td>
    {{$voter->district}}
    </td>

    </tr>
  

  </table>
@endsection