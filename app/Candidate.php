<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    public function party()
    {
        return $this->belongsTo('App\Party');
    }
    public function voters()
    {
        return $this->belongsToMany('App\Voter');
    }
}
