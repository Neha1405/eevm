@extends('layouts.masterlayout')
@section('content')
 
<h1>All Voters</h1>
   
    <a href="{{route('voters.create')}}" class="btn btn-info" style="margin-left:700px">Add new Voter</a>

    <table class="table table-dark" style="margin-top:50px">

    <tr>
      <th>Voter ID</th>
      <th>Voter Name</th>
      <th>State</th>
      <th>District</th>
      <th>Action</th>
    
    </tr>

@foreach($voters as $voter)
<tr>
    <td>{{$voter->id}}</td>
    <td>
    {{$voter->name}}
    </td>
    <td>
    {{$voter->state}}
    </td>
    <td>
    {{$voter->district}}
    </td>

    <td class="form-group row" >
    
    <a href="{{route('voters.show',$voter)}}" ><i class="fas fa-eye" style="margin-left:50px;color:#00BFFF"></i></button></a>
  
    <a href="{{route('voters.edit',$voter)}}" ><i class="fas fa-edit" style="margin-left:50px;color:#00BFFF"></i></button></a>
 
    
   
    <form action="{{route('voters.destroy',$voter)}}" method="POST">
    @csrf()
    @method('delete')
    <button style="margin-left:40px;color:#00BFFF; background-color:transparent; border:none" class="fas fa-trash"></button></td>
    </form>

    </tr>
    @endforeach

  </table>
@endsection