<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Party;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates=Candidate::all();
        return view('Candidate.index')->withCandidates($candidates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parties=Party::all();
        return view('Candidate.create')->withParties($parties);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate=new Candidate;
        $candidate->name=$request->name;
        $candidate->state=$request->state;
        $candidate->district=$request->district;
        $candidate->party_id=$request->party_id;
        $candidate->save();
        
        return redirect()->route('candidates.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate)
    {
        return view('Candidate.show')->withCandidate($candidate);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit(Candidate $candidate)
    {
        return view('Candidate.edit')->withCandidate($candidate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        $candidate->name=$request->name;
        $candidate->state=$request->state;
        $candidate->district=$request->district;
        $candidate->party=$request->party;

        $candidate->save();
        return redirect()->route('candidates.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
        $candidate->delete();
        return redirect()->route('candidates.index');
    }
}