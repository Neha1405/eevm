<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::middleware(['auth','admins'])->group(function()
//{
Route::resource('candidates', 'CandidateController');
Route::resource('voters', 'VoterController');
Route::resource('parties', 'PartyController');
//});
Route::get('votings/voter_login','VotingPannelController@voter_login')->name('votings.voter_login');
Route::get('votings/index','VotingPannelController@index')->name('votings.index');
Route::get('votings/voter_auth','VotingPannelController@voter_auth')->name('votings.voter_auth');
Route::get('votings/final','VotingPannelController@final')->name('votings.final');
Route::get('votings/vote_save','VotingPannelController@vote_save')->name('votings.vote_save');
